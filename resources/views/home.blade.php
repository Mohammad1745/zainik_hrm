@extends(\Auth::user() ?'layouts.user' : 'layouts.auth')

@section('content')
    @if(\Auth::user())
        <div class="card mt-5 p-3">
            <div class="">
                <div>Public Profile Link:</div>
                <div class="public-link" id="public_link">{{route('public_link', \Auth::id())}}</div>
                <button onclick="copyContent(this)" class="btn btn-outline-info">Click to copy</button>
            </div>
        </div>
    @else
        <div class="d-flex justify-content-center">
            <div class="card mt-5 p-3">
                <div class="card-body d-flex">
                    <div class="nav-item">
                        <a class="nav-link" aria-current="page" href="{{route('login')}}">Login</a>
                    </div>
                    <div class="nav-item">
                        <a class="nav-link" aria-current="page" href="{{route('registration')}}">Register</a>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('style')
    <style>
        .public-link {
            color: #1a202c;
            font-weight: bold;
        }
    </style>
@endsection

@section('script')
    <script>
        let text = document.getElementById('public_link').innerHTML;
        const copyContent = async (el) => {
            try {
                await navigator.clipboard.writeText(text);
                el.innerText = 'Copied!';
                setTimeout(() => {
                    el.innerText = 'Click to Copy!';
                }, 5000)
            } catch (err) {
                console.error('Failed to copy: ', err);
            }
        }
    </script>
@endsection
