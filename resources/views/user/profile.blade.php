<!doctype html>
<html lang="en">
@if(isset($user))
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>{{$user->name}}</title>
        <!-- Search Engine -->
        <meta name="image" content="{{asset($user->avatar)}}">
        <meta name="description" content="{{ $user->about }}">
        <!-- Schema.org for Google -->
        <meta itemprop="image" content="{{asset($user->avatar)}}">
        <meta itemprop="name" content="Name: {{$user->name}} Email: {{$user->email}}">
        <meta itemprop="description" content="{{ $user->about }}">
        <!-- Open Graph general (Facebook, Pinterest & Google+) -->
        <meta name="og:title" content="Name: {{$user->name}} Email: {{$user->email}}">
        <meta name="og:image" content="{{asset($user->avatar)}}">
        <meta name="og:url" content="https://zhrm.aliii.me/profile/{{$user->id}}">
        <meta name="og:site_name" content="Name: {{$user->name}} Email: {{$user->email}}">
        <meta name="og:type" content="website">
        <meta name="og:description" content="{{ $user->about }}">
    </head>
@else
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Zainik HRM</title>
        <!-- Search Engine -->
        <meta name="description" content="HRM">
        <!-- Schema.org for Google -->
        <meta itemprop="name" content="Zainik HRM">
        <meta itemprop="description" content="HRM">
        <!-- Open Graph general (Facebook, Pinterest & Google+) -->
        <meta name="og:title" content="Zainik HRM">
        <meta name="og:url" content="https://zhrm.aliii.me/register">
        <meta name="og:site_name" content="Zainik HRM">
        <meta name="og:type" content="website">
        <meta name="og:description" content="HRM">
    </head>
@endif
<body>
    <script>
        window.addEventListener('DOMContentLoaded', () => {
            setTimeout(() => {
                window.location.href=window.location.origin+'/register'
            },100)
        })
    </script>
</body>
</html>
