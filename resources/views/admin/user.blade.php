@extends('layouts.admin')

@section('title', 'Users | Admin')

@section('content')
    <div class="card mt-5 p-3 pb-0">
        <table>
            <thead>
            <tr>
                <th>Avatar</th>
                <th>Name</th>
                <th>Email</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr class="border-bottom">
                    <td><img src="{{ asset($user->avatar) }}" alt="" width="50px"></td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        <span
                            class="p-2 rounded text-white"
                            style="background: {{userStatusColor($user->status)}}">
                            {{ userStatus($user->status) }}
                        </span>
                    </td>
                    <td>
                        <button onclick="showDetails('{{ $user->id }}')" class="btn btn-primary" title="Disable">
                            <i class="fas fa-info"></i>
                        </button>

                        <div id="details_{{ $user->id }}" class="details">
                            <div class="card">
                                <div class="card-header d-flex justify-content-between">
                                    <div>{{ $user->name }}</div>
                                    <button class="btn btn-primary" onclick="hideDetails('{{ $user->id }}')"><i class="fa fa-times"></i></button>
                                </div>
                                <div class="card-body d-flex flex-column gap-2">
                                    <div><img src="{{ asset($user->avatar) }}" alt="" width="100%"></div>
                                    <div>Name: {{ $user->name }}</div>
                                    <div>Email: {{ $user->email }}</div>
                                    <div>
                                        Status:
                                        <span
                                            class="p-2 rounded text-white"
                                            style="background: {{userStatusColor($user->status)}}">
                                            {{ userStatus($user->status) }}
                                        </span>
                                    </div>
                                    <div>
                                        About:
                                        <p class="text-muted small">{{ $user->about }}</p>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    @if($user->status==USER_STATUS_ACTIVE)
                                        <a href="{{route('admin.user.disable', $user->id)}}" class="btn btn-primary" title="Disable">
                                            <i class="fas fa-ban"></i>
                                        </a>
                                    @else
                                        <a href="{{route('admin.user.enable', $user->id)}}" class="btn btn-primary" title="Enable">
                                            <i class="fas fa-check"></i>
                                        </a>
                                    @endif
                                    <a href="{{route('admin.user.remove', $user->id)}}" class="btn btn-danger" title="Remove">
                                        <i class="fas fa-trash"></i>
                                    </a>
                                </div>
                            </div>
                        </div>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination mt-2 d-flex justify-content-between align-items-center">
            <div class="text-muted">
                Showing {{ $users->firstItem() }} to {{ $users->lastItem() }} of {{ $users->total() }} records
            </div>
            <div class="d-flex">
                {{ $users->links('pagination::bootstrap-4') }}
            </div>
        </div>
    </div>
@endsection

@section('style')
    <style>
        .details {
            display: none;
            position: fixed;
            top:0;
            left: 0;
            width: 100vw;
            height: 100vh;
            background: rgba(74, 85, 104, 0.2);
            justify-content: center;
            align-items: center;
            overflow-y: scroll;
        }
        .details .card {
            max-width: 450px;
        }
    </style>
@endsection

@section('script')
    <script>
        function showDetails(userId) {
            const details = document.getElementById(`details_${userId}`);
            details.style.display = 'flex'
        }

        function hideDetails(userId) {
            const zoomedImage = document.getElementById(`details_${userId}`);
            zoomedImage.style.display = 'none';
        }
    </script>
@endsection
