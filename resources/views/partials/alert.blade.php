@if(session()->has('success'))
    <div class="alert alert-success">{{session()->get('success')}}</div>
@endif
@if(session()->has('error'))
    <div class="alert alert-danger">{{session()->get('error')}}</div>
@endif

<script>
    window.addEventListener('DOMContentLoaded', () => {
        setTimeout(() => {
            const alert = document.querySelector('.alert')
            if (alert) alert.remove()
        },2500)
    })
</script>
