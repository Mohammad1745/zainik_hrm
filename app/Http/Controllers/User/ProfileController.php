<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Services\Feature\UserService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class ProfileController extends Controller
{
    /**
     * @param UserService $service
     */
    function __construct (private readonly UserService $service) {}

    /**
     * @param string $id
     * @return Application|Factory|View
     */
    public function index (string $id): View|Factory|Application
    {
        $response = $this->service->getInfo($id);
        return view('user.profile', $response['data']);
    }
}
