<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegistrationRequest;
use App\Http\Services\Feature\AuthService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class AuthController extends Controller
{
    /**
     * @param AuthService $service
     */
    function __construct (private readonly AuthService $service) {}

    /**
     * @return Application|Factory|View
     */
    public function login (): View|Factory|Application
    {
        return view('auth.login');
    }

    /**
     * @param LoginRequest $request
     * @return RedirectResponse
     */
    public function processLogin (LoginRequest $request): RedirectResponse
    {
        $response = $this->service->processLogin($request->all());

        return $response['success'] ?
            redirect()
                ->route($response['data']['type'] == 'admin' ? 'admin.dashboard' : 'home')
                ->with('success', $response['message'])
            : redirect()->back()->with('error', $response['message']);
    }

    /**
     * @return Application|Factory|View
     */
    public function register (): View|Factory|Application
    {
        return view('auth.register');
    }

    /**
     * @param RegistrationRequest $request
     * @return RedirectResponse
     */
    public function processRegistration (RegistrationRequest $request): RedirectResponse
    {
        $response = $this->service->processRegistration($request->all());

        return $response['success'] ?
            redirect()->route('login')->with('success', $response['message'])
            : redirect()->back()->with('error', $response['message']);
    }

    /**
     * @return RedirectResponse
     */
    public function logout (): RedirectResponse
    {
        $response = $this->service->logout();

        return $response['success'] ?
            redirect()->route('home')->with('success', $response['message'])
            : redirect()->back()->with('error', $response['message']);
    }

    /**
     * @return RedirectResponse
     */
    public function logoutAdmin (): RedirectResponse
    {
        $response = $this->service->logoutAdmin();

        return $response['success'] ?
            redirect()->route('home')->with('success', $response['message'])
            : redirect()->back()->with('error', $response['message']);
    }
}
