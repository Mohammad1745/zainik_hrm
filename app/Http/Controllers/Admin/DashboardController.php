<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Services\Feature\AuthService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class DashboardController extends Controller
{
    /**
     * @param AuthService $service
     */
    function __construct (private readonly AuthService $service) {}

    /**
     * @return Application|Factory|View
     */
    public function index (): View|Factory|Application
    {
        return view('admin.dashboard');
    }
}
