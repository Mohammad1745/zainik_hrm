<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Services\Feature\UserService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class UserController extends Controller
{
    /**
     * @param UserService $service
     */
    function __construct (private readonly UserService $service) {}

    /**
     * @return Application|Factory|View
     */
    public function index (): View|Factory|Application
    {
        $response = $this->service->getList();
        return $response['success'] ?
            view('admin.user', $response['data'])
            : view('admin.user', []);
    }

    /**
     * @param string $id
     * @return RedirectResponse
     */
    public function enable (string $id): RedirectResponse
    {
        $response = $this->service->enable($id);

        return $response['success'] ?
            redirect()->route('admin.user.index')->with('success', $response['message'])
            : redirect()->back()->with('error', $response['message']);
    }

    /**
     * @param string $id
     * @return RedirectResponse
     */
    public function disable (string $id): RedirectResponse
    {
        $response = $this->service->disable($id);

        return $response['success'] ?
            redirect()->route('admin.user.index')->with('success', $response['message'])
            : redirect()->back()->with('error', $response['message']);
    }

    /**
     * @param string $id
     * @return RedirectResponse
     */
    public function remove (string $id): RedirectResponse
    {
        $response = $this->service->remove($id);

        return $response['success'] ?
            redirect()->route('admin.user.index')->with('success', $response['message'])
            : redirect()->back()->with('error', $response['message']);
    }
}
