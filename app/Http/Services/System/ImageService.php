<?php

namespace App\Http\Services\System;

use Intervention\Image\Facades\Image;
use Intervention\Image\Image as InterventionImage;

class ImageService
{

    /**
     * @param $image
     * @param string $path
     * @param string|null $oldKey
     * @param string $format
     * @param int $defaultWidth
     * @return string
     * @throws \Exception
     */
    public function uploadImage ($image, string $path, string $oldKey=null, string $format='jpg', int $defaultWidth=1920): string
    {
        $imageName = $oldKey ?
            lastElement(explode('-', $oldKey))
            : str_replace('.', '', microtime(true)) . mt_rand(100000, 999999) . mt_rand(1000000, 9999999) . "." . $format;

        $image = Image::make($image);
        $image->resizeCanvas($image->width(), $image->height());

        $file = $this->_optimizeImageQuality($image, $defaultWidth, $format);
        $location = $path.$imageName;
        $file->save($location);

        return $location;
    }

    /**
     * @param InterventionImage $image
     * @param int $maxWidth
     * @param string $format
     * @param int $quality
     * @return InterventionImage
     */
    private function _optimizeImageQuality (InterventionImage $image, int $maxWidth=1920, string $format='jpg', int $quality=100): InterventionImage
    {
        $optimizedImage = clone $image;
        $imageWidth = $optimizedImage->width();
        $imageHeight = $optimizedImage->height();

        $optimizedImage = $format == 'jpg' ?
            $optimizedImage->encode($format, $quality)
            : $optimizedImage->encode($format);
        $optimizedImage = Image::make($optimizedImage);

        $multiplier = $imageWidth > $maxWidth ? $maxWidth/$imageWidth : 1;
        $optimizedImage->resize(intval($imageWidth*$multiplier), intval($imageHeight*$multiplier));

        return $optimizedImage;
    }
}
