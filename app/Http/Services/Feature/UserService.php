<?php

namespace App\Http\Services\Feature;

use App\Models\Customer;
use App\Traits\Response;

class UserService
{
    use Response;

    /**
     * @return array
     */
    public function getList (): array
    {
        try {
            $users = Customer::paginate(10);

            return $this->response(['users' => $users])->success();
        }
        catch (\Exception $exception) {
            return $this->response()->error( $exception->getMessage());
        }
    }

    /**
     * @param string $id
     * @return array
     */
    public function getInfo (string $id): array
    {
        try {
            $user = Customer::find($id);
            if (!$user) {
                return $this->response()->error("User not found!");
            }
            return $this->response(['user' => $user])->success();
        }
        catch (\Exception $exception) {
            return $this->response()->error( $exception->getMessage());
        }
    }

    /**
     * @param string $id
     * @return array
     */
    public function enable (string $id): array
    {
        try {
            $user = Customer::find($id);
            if (!$user) {
                return $this->response()->error("User not found!");
            }
            $user->update(['status' => USER_STATUS_ACTIVE]);

            return $this->response()->success("User has been enabled successfully!");
        }
        catch (\Exception $exception) {
            return $this->response()->error( $exception->getMessage());
        }
    }

    /**
     * @param string $id
     * @return array
     */
    public function disable (string $id): array
    {
        try {
            $user = Customer::find($id);
            if (!$user) {
                return $this->response()->error("User not found!");
            }
            $user->update(['status' => USER_STATUS_DISABLED]);

            return $this->response()->success("User has been disabled successfully!");
        }
        catch (\Exception $exception) {
            return $this->response()->error( $exception->getMessage());
        }
    }

    /**
     * @param string $id
     * @return array
     */
    public function remove (string $id): array
    {
        try {
            $user = Customer::find($id);
            if (!$user) {
                return $this->response()->error("User not found!");
            }
            if (file_exists($user->avatar)) {
                unlink($user->avatar);
            }
            $user->delete();

            return $this->response()->success("User has been removed successfully!");
        }
        catch (\Exception $exception) {
            return $this->response()->error( $exception->getMessage());
        }
    }
}
