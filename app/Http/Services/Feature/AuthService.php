<?php

namespace App\Http\Services\Feature;

use App\Http\Services\System\ImageService;
use App\Models\Admin;
use App\Models\Customer;
use App\Models\FailedLoginAttempt;
use App\Traits\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthService
{
    use Response;

    /**
     * @param ImageService $imageService
     */
    function __construct (private readonly ImageService $imageService) {}

    /**
     * @param array $data
     * @return array
     */
    public function processLogin (array $data): array
    {
        try {
            $user = Customer::where('email', $data['email'])->first();
            if ($user) {
                $blockedTill = $this->_checkBlockedUser($data['email']);
                if ($blockedTill) {
                    return $this->response()->error("You are blocked! Please try again after $blockedTill");
                }

                if (!Hash::check($data['password'], $user->password)) {
                    return $this->_countFailedLoginAttempt($data['email']) ?
                        $this->response()->error("You are blocked! Please try again after $blockedTill")
                        : $this->response()->error("Wrong Email Or Password");
                }
                Auth::guard('user')->login($user);
                $this->_resetAttempts($data['email']);

                return $this->response(['type' => 'user'])->success("Login Successful!");
            }

            $admin = Admin::where('email', $data['email'])->first();
            if ($admin) {
                $blockedTill = $this->_checkBlockedUser($data['email']);
                if ($blockedTill) {
                    return $this->response()->error("You are blocked! Please try again after $blockedTill");
                }

                if (!Hash::check($data['password'], $admin->password)) {
                    return $this->_countFailedLoginAttempt($data['email']) ?
                        $this->response()->error("You are blocked! Please try again after $blockedTill")
                        : $this->response()->error("Wrong Email Or Password");
                }
                Auth::guard('admin')->login($admin);
                $this->_resetAttempts($data['email']);

                return $this->response(['type' => 'admin'])->success("Login Successful!");
            }
            else {
                return $this->response()->error("Email not found");
            }
        }
        catch (\Exception $exception) {
            return $this->response()->error( $exception->getMessage());
        }
    }

    /**
     * @param array $data
     * @return array
     */
    public function processRegistration (array $data): array
    {
        try {
            $avatarLocation = $this->imageService->uploadImage($data['avatar'], 'uploads/avatar/', null,'jpg', 800);
            Customer::create( $this->_formatUserData($data, $avatarLocation));

            return $this->response()->success("Registration Successful!");
        }
        catch (\Exception $exception) {
            return $this->response()->error( $exception->getMessage());
        }
    }

    /**
     * @return array
     */
    public function logout (): array
    {
        try  {
            Auth::guard('user')->logout();
            return $this->response()->success("Logged Out Successful!");
        }
        catch (\Exception $exception) {
            return $this->response()->error( $exception->getMessage());
        }
    }

    /**
     * @return array
     */
    public function logoutAdmin (): array
    {
        try  {
            Auth::guard('admin')->logout();
            return $this->response()->success("Logged Out Successful!");
        }
        catch (\Exception $exception) {
            return $this->response()->error( $exception->getMessage());
        }
    }

    /**
     * @param $email
     * @return string|null
     */
    private function _countFailedLoginAttempt ($email): string|null
    {
        $loginAttempt = FailedLoginAttempt::where('email', $email)->first();

        if ($loginAttempt) {
            $loginAttempt->attempt_count++;
            $loginAttempt->save();
        } else {
            FailedLoginAttempt::create([
                'email' => $email,
                'attempt_count' => 1,
                'last_attempt_at' => now(),
            ]);
        }

        $loginAttempt = FailedLoginAttempt::where('email', $email)->first();
        if ($loginAttempt->attempt_count >= 6) {
            $loginAttempt->blocked_until = now()->addMinutes(45);
            $loginAttempt->save();
            return now()->addMinutes(45);
        }
        if ($loginAttempt->attempt_count == 3) {
            $loginAttempt->blocked_until = now()->addMinutes(15);
            $loginAttempt->save();
            return now()->addMinutes(15);
        }
        return null;
    }

    /**
     * @param $email
     * @return string|null
     */
    private function _checkBlockedUser ($email): string|null
    {
        $loginAttempt = FailedLoginAttempt::where('email', $email)->first();
        if (!$loginAttempt) return null;

        return $loginAttempt->blocked_until >= Carbon::now() ?
            $loginAttempt->blocked_until
            : null;
    }

    /**
     * @param $email
     * @return void
     */
    private function _resetAttempts ($email): void
    {
        FailedLoginAttempt::where('email', $email)->delete();
    }

    /**
     * @param array $data
     * @param $avatarLocation
     * @return array
     */
    private function _formatUserData (array $data, $avatarLocation): array
    {
        return [
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'avatar' => $avatarLocation,
            'about' => $data['about'],
        ];
    }
}
