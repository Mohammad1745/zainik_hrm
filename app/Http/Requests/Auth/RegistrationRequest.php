<?php

namespace App\Http\Requests\Auth;

use App\Rules\ReCaptchaRule;
use Illuminate\Foundation\Http\FormRequest;

class RegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'email' => 'required|email|unique:customers',
            'password' => 'required|min:6',
            'password_confirmation' => 'required|same:password',
            'about' => 'required',
            'avatar' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'g-recaptcha-response' => ['required', new ReCaptchaRule],
        ];
    }

    /**
     * @return string[]
     */
    public function messages(): array
    {
        return [
            'g-recaptcha-response' => 'Please select the recaptcha!'
        ];
    }
}
