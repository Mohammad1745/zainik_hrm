<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FailedLoginAttempt extends Model
{
    use HasFactory, Uuid;

    /**
     * @var string[]
     */
    protected $fillable = [
        'email',
        'attempt_count',
        'blocked_until'
    ];
}
