<?php

/**
 * @param array $array
 * @return mixed|null
 */
function lastElement (array $array)
{
    if (count($array)==0) return null;
    return $array[count($array)-1];
}

/**
 * @param $key
 * @return string|string[]
 */
function userStatus ($key=null): array|string
{
    $output = [
        USER_STATUS_DISABLED => 'Disabled',
        USER_STATUS_ACTIVE => 'Active',
    ];
    if (is_null($key)) {
        return $output;
    } else {
        return $output[$key];
    }
}

/**
 * @param $key
 * @return string|string[]
 */
function userStatusColor ($key=null): array|string
{
    $output = [
        USER_STATUS_DISABLED => '#833',
        USER_STATUS_ACTIVE => '#3a4',
    ];
    if (is_null($key)) {
        return $output;
    } else {
        return $output[$key];
    }
}
