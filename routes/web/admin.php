<?php

use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;

//Auth
Route::group(['prefix'=> 'admin', 'as'=>'admin.', 'middleware'=>['auth.admin']], function () {
    Route::get('/logout', [AuthController::class, 'logoutAdmin'])->name('logout');

    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

    Route::group(['prefix'=> 'user', 'as'=>'user.'], function () {
        Route::get('/', [UserController::class, 'index'])->name('index');
        Route::get('/enable/{id}', [UserController::class, 'enable'])->name('enable');
        Route::get('/disable/{id}', [UserController::class, 'disable'])->name('disable');
        Route::get('/remove/{id}', [UserController::class, 'remove'])->name('remove');
    });
});
