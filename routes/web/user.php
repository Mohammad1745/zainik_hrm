<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\User\ProfileController;
use Illuminate\Support\Facades\Route;

//Auth
Route::group(['middleware'=>['auth']], function () {
    Route::get('/logout', [AuthController::class, 'logout'])->name('logout');

});

Route::get('/u/{id}', [ProfileController::class, 'index'])->name('public_link');
