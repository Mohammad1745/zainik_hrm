<?php

use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;

//Auth
Route::get('/login', [AuthController::class, 'login'])->name('login');
Route::post('/login', [AuthController::class, 'processLogin'])->name('login.process');
Route::get('/register', [AuthController::class, 'register'])->name('registration');
Route::post('/register', [AuthController::class, 'processRegistration'])->name('registration.process');
